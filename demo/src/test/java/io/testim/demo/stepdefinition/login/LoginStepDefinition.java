package io.testim.demo.stepdefinition.login;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.testim.demo.model.login.LoginModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static io.testim.demo.question.login.ValidateLogin.validateLogin;
import static io.testim.demo.task.landingpage.GoToLandingPage.goToLandingPage;
import static io.testim.demo.task.login.DoLogin.doLogin;
import static io.testim.demo.task.login.GoToLogin.goToLogin;
import static org.hamcrest.Matchers.is;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class LoginStepDefinition {

    private final Actor asociado = new Actor("Asociado");

    @Managed
    private WebDriver webDriver;

    @Dado("que el asociado se encuentra en la página de inicio de sesión")
    public void queElAsociadoSeEncuentraEnLaPaginaDeInicioDeSesion() {
        setUpBroser();
        configureActor();
        asociado.wasAbleTo(
                goToLandingPage(),
                goToLogin()
        );
    }

    @Cuando("el asociado efectúa el inicio de sesión")
    public void elAsociadoEfectuaElInicioDeSesion() {
        asociado.attemptsTo(
                doLogin().usingTheUserCredentials(credentials())
        );
    }

    @Entonces("el asociado será reconocido en el sistema.")
    public void elAsociadoSeraReconocidoEnElSistema() {
        asociado.should(
                seeThat(
                        validateLogin().withTheUserName(credentials().getUserName()),
                        is(true)
                )
        );
    }

    private LoginModel credentials(){
        LoginModel loginModel = new LoginModel();
        loginModel.setUserName("JOHN");
        loginModel.setPassword("1234");

        return loginModel;
    }

    private void setUpBroser(){
        webDriver.manage().window().maximize();
    }

    private void configureActor(){
        asociado.can(BrowseTheWeb.with(webDriver));
    }
}
