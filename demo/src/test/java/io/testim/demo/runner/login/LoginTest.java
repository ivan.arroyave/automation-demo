package io.testim.demo.runner.login;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/feature/login/login.feature"},
        glue = {"io.testim.demo.stepdefinition.login"},
        tags = "@RegressionTest"
)
public class LoginTest {
}
