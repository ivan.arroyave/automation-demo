#language: es

Característica: Inicio de sesión
  Como usuario de Space Advisor
  necesito iniciar sesión en el portal web
  Con el fin de poder acceder a las funcionalides de los asociados.

  @RegressionTest
  Escenario: Inicio de sesión exitoso
    Dado que el asociado se encuentra en la página de inicio de sesión
    Cuando el asociado efectúa el inicio de sesión
    Entonces el asociado será reconocido en el sistema.