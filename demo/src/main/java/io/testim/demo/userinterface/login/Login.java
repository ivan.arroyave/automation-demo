package io.testim.demo.userinterface.login;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.*;

public class Login extends PageObject {
    public static final Target LOGIN = Target
            .the("LOG IN")
            .located(className("NavButton__nav-button___34wHC"));

    public static final Target USER_NAME = Target
            .the("Username")
            .located(xpath("//*[@id=\"login\"]/div[1]/input"));

    public static final Target PASSWORD = Target
            .the("Password")
            .located(cssSelector("input[type='password']"));

    public static final Target LOGIN_BUTTON = Target
            .the("LOG IN")
            .located(cssSelector("button[form='login']"));

    public static final Target USER_HELLO_BUTTON = Target
            .the("HELLO button")
            .located(cssSelector("button[class='mui-btn mui-btn--primary ']"));
}
