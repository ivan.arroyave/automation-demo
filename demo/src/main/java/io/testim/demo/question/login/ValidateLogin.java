package io.testim.demo.question.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static io.testim.demo.userinterface.login.Login.USER_HELLO_BUTTON;

public class ValidateLogin implements Question<Boolean> {

    private String name;

    public ValidateLogin withTheUserName(String userName) {
        this.name = userName;
        return this;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return USER_HELLO_BUTTON.resolveFor(actor).containsText(name);
    }

    public static ValidateLogin validateLogin(){
        return new ValidateLogin();
    }
}
