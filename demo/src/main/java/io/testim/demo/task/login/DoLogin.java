package io.testim.demo.task.login;

import io.testim.demo.model.login.LoginModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static io.testim.demo.userinterface.login.Login.*;

public class DoLogin implements Task {

    private LoginModel credentials;

    public DoLogin usingTheUserCredentials(LoginModel credentials) {
        this.credentials = credentials;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(credentials.getUserName()).into(USER_NAME),
                Enter.theValue(credentials.getPassword()).into(PASSWORD),
                Click.on(LOGIN_BUTTON)
        );
    }

    public static DoLogin doLogin(){
        return new DoLogin();
    }
}
