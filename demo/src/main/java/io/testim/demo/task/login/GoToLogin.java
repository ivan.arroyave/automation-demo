package io.testim.demo.task.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static io.testim.demo.userinterface.login.Login.LOGIN;

public class GoToLogin implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LOGIN)
        );
    }

    public static GoToLogin goToLogin(){
        return new GoToLogin();
    }
}
