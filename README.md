# Demo de automatización

Ejemplo académico de automatización de pruebas usando Serenity BDD y el patrón Screenplay.

## Comenzando 🚀
Ten presente las siguientes configuraciones para minimizar los inconvenientes en el momento de ejecución.

### Pre-requisitos 📋

* [JetBrains Toolbox - IntelliJ IDEA Community Edition.](https://www.jetbrains.com/es-es/lp/toolbox/)
* * Configurar plugins:
* * * Cucumber for Java.
* * * Gherkin.
* * * SonarLint.
* * * Substeps Intellij Plugin.
* * * Gradle.
* [Java 8 - JDK.](https://www.oracle.com/co/java/technologies/javase/javase8-archive-downloads.html)
* [Archivos binarios de Gradle - Se recomienda el release 7.3 para generar bien el reporte de pruebas.](https://gradle.org/releases/)
* [Git](https://git-scm.com/downloads)

### Abrir el proyecto en IntelliJ IDEA 📋
![img_11.png](img_11.png)

Debe abrir el proyecto desde el archivo build.gradle y además como proyecto (NO COMO ARCHIVO).

![img_12.png](img_12.png)


### Instalar plugins 🔧

![img.png](img.png)

```
Una vez que identifique la sección de Plugins debe ir al Marketplace y buscar los Plugins señalados. 
Cuando finalice dicho proceso, podrá ver los Plugins en la sección Installed.
```

![img_1.png](img_1.png)


### Antes de ejecutar el ejemplo de automatización 🔧
![img.png](img.png)

```
Es necesario indicar que los runners deben ejecutarse con IntelliJ IDEA y no 
con Gradle (como aparece por lo general por defecto).
```

![img_2.png](img_2.png)


## Ejecutando las pruebas ⚙️
Ejecuta la clase runner del test.

![img_3.png](img_3.png)

### Generar evidencias 📄�?
Ejecutar el comando 'gradle aggregate' en la terminal de IntelliJ IDEA. Para este ejemplo se recomienda tener configurado el en equipo los archivos binarios de gradle en su versión 7.3.

![img_9.png](img_9.png)
```
Las evidencias quedarán generadas en la carpeta 'target'. Debes ejecutar al menos una vez la automatización.
Busca y abre el archivo 'index.html' que debe estar en la subcarpeta 'serenity'.
```
![img_8.png](img_8.png)

Las evidencias tendrán este aspecto:

![img_10.png](img_10.png)